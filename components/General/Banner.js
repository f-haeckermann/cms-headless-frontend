import arrow from '../../public/assets/arrow.svg';
import Image from 'next/image';

const Banner = ({ banner }) => {
  const mybanner = banner.data.banner;
  return (
    <div className="row slider min-vh-100">
      <div className="col-12 position-relative">
        <div className="position-absolute title">
          Herausragendes Resultat im Geschäftsjahr 2020
        </div>
        <div className="position-absolute check">
          <Image src={arrow} width={30} alt="Arrow" />
        </div>
        <Image
          className="myImage"
          src={mybanner.url}
          layout="fill"
          objectFit="cover"
          alt={mybanner.alt}
        />
      </div>
    </div>
  );
};
export default Banner;
