const Footer = () => (
  <div className="container-fluid w-100">
    <footer className="row">
      <nav className="navbar navbar-expand-sm navbar-dark bg">
        <div className="d-flex flex-row justify-content-between align-items-center w-100">
          <a className="navbar-brand fs-6" href="#">
            &copy; 2022 LINKGROUP
          </a>
          <ul className="navbar-nav d-flex align-items-center flex-row ms-auto">
            <li className="nav-item px-1">
              <a className="nav-link active " aria-current="page" href="#">
                Impressum
              </a>
            </li>
            <li className="nav-item px-1">
              <a className="nav-link" href="#">
                Disclaimer
              </a>
            </li>
          </ul>
        </div>
      </nav>
    </footer>
  </div>
);

export default Footer;
