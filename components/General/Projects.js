import Link from 'next/link';
import Image from 'next/image';

const Projects = ({ projects }) => (
  <div className="row list mt-4">
    {projects.map((myproject) => {
      debugger;
      const [desc] = myproject.data.description;
      const [name] = myproject.data.name;
      const city = myproject.data.city;
      const id = myproject.id;
      const image = myproject.data.image;
      const uid = myproject.uid;
      return (
        <div key={id} className="col-12 col-md-6 position-relative">
          <Link href={'/projects/' + uid}>
            <a className="stretched-link"></a>
          </Link>
          <div className="card border-0 w-100">
            <Image
              layout="responsive"
              src={image.url}
              width={image.dimensions.width}
              height={image.dimensions.height}
              alt={image.alt}
            />
            <div className="mt-3">
              <p className="text-tiny mb-0">
                {name.text}, {city}
              </p>
              <p className="fs-5 lh-sm">{desc.text}</p>
            </div>
          </div>
        </div>
      );
    })}
  </div>
);
export default Projects;
