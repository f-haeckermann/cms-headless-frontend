const NavBar = () => (
  <div className="col-2 mb-4">
    <div className="position-fixed top ">
      <div className="d-flex h-100 flex-column align-items-center">
        <div className="flex-grow-1 rotate text-end pb-4">
          <a href="#" title="Linkgroup Home Page" target="_self" className="h2">
            Linkgroup
          </a>
        </div>
        <div className="mb-4 rotate mt-4">
          <a
            href="#"
            title="Linkgroup Reporting Page"
            target="_self"
            className="h3 text-decoration-none btn btn-link text-secondary "
          >
            Reporting
          </a>
        </div>
      </div>
    </div>
  </div>
);

export default NavBar;
