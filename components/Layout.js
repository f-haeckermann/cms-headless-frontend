import NavBar from './General/NavBar';
import Footer from './General/Footer';

const Layout = (props) => (
  <div className="Layout mt-4">
    <div className="container-fluid w-100">
      <div className="row">
        {/*************************/}
        {/* Container Sidebar & Content */}
        {/*************************/}
        <div className="container-fluid w-100">
          <div className="row g-0">
            {/*************************/}
            {/* Container Sidebar */}
            {/*************************/}
            <NavBar />

            {/*************************/}
            {/* Container Main Content */}
            {/*************************/}
            {props.children}
          </div>
        </div>
        {/*************************/}
        {/* Container Footer */}
        {/*************************/}
        <Footer />
      </div>
    </div>
  </div>
);

export default Layout;
