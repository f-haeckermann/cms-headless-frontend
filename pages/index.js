import Layout from '../components/Layout';
import Banner from '../components/General/Banner';
import Projects from '../components/General/Projects';
import { createClient } from '../prismicio';

export const getStaticProps = async (context) => {
  const client = createClient({ context });
  const mybanner = await client.getSingle('banner');
  const myprojects = await client.getAllByType('projects');

  return {
    props: {
      mybanner,
      myprojects,
    },
  };
};

export default function Home({ mybanner, myprojects }) {
  return (
    <Layout>
      <div className="col-10">
        <Banner banner={mybanner} />
        <Projects projects={myprojects} />
      </div>
    </Layout>
  );
}
