import React from 'react';
import Image from 'next/image';
import Link from 'next/link';
// PRISMIC
import * as prismicH from '@prismicio/helpers';
import { createClient, linkResolver } from '../../prismicio';
//Own cmps
import Layout from '../../components/Layout';

export default function ProjectView({ myproject }) {
  debugger;
  const Name = prismicH.asText(myproject.data.name);
  const Description = prismicH.asText(myproject.data.description);
  const City = myproject.data.city;
  const Picture = myproject.data.image;
  return (
    <Layout className="text-tiny mb-0 ">
      <div className="col-10 min-vh-100 d-flex align-items-center ">
        <div className="row w-100 my-4">
          <div className="col-12 col-md-4">
            <Image
              src={Picture.url}
              alt={Picture.alt}
              layout="responsive"
              width={Picture.dimensions.width}
              height={Picture.dimensions.height}
            />
          </div>
          <div className="col-12 col-md-8">
            <div className="row">
              <div className="col-12">
                <Link href={'/'}>
                  <a className="btn btn-link p-0">Back to Home</a>
                </Link>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <div className="fs-3">{City}</div>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <div className="display-5">{Name}</div>
              </div>
            </div>

            <div className="row mt-4">
              <div className="col-12">
                <div className="fs-6">{Description}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export const getStaticProps = async ({ params, previewData }) => {
  const client = createClient({ previewData });
  const myproject = await client.getByUID('projects', params.uid);
  return {
    props: {
      myproject,
    },
  };
};

export const getStaticPaths = async () => {
  const client = createClient();
  const projects = await client.getAllByType('projects');
  return {
    paths: projects.map((doc) => prismicH.asLink(doc, linkResolver)),
    fallback: true,
  };
};
